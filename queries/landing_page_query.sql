
-- ------------------
-- This script contains the queries to retrieve data against the "landing page of the application
-- The landing page will contain 2 types of information
-- namely details about the user (his name, email, channels he is part of or is subscribed to)
-- and recommended videos (from his watch later and and videos released by the channels he subscibed to)
-- -------------------

-- ----------------------------
-- 1.1 user details query.
-- ----------------------------
SET
@LoggedInUserID = 5;

SELECT
	firstname,
	lastname,
	username,
	email,
	about,
	gender_id
FROM
	noob_tube.`user` as u
WHERE
	u.id = @LoggedInUserID;
-- ----------------------------
-- 1.1 user subscribed channel query.

-- ----------------------------
 SELECT
	s.user_id,
	s.id AS subscription_id,
	s.channel_id,
	c.name AS channel_name,
	s.subscription_time
FROM
	noob_tube.subscription AS s
INNER JOIN noob_tube.channel AS c ON
	s.channel_id = c.id
WHERE
	s.user_id = @LoggedInUserID
ORDER BY
	c.name ASC;
-- ----------------------------
-- 1.2 user channel member query.
-- ----------------------------
 SET
@LoggedInUserID = 137;

SELECT
	cm.user_id,
	cm.id AS channel_member_id,
	cm.channel_id,
	c.name,
	(
	SELECT
		role_type
	FROM
		noob_tube.`role`
	WHERE
		id = cm.role_id) AS role_type
FROM
	noob_tube.channel_member AS cm
INNER JOIN noob_tube.channel AS c ON
	cm.channel_id = c.id
WHERE
	cm.user_id = @LoggedInUserID
ORDER BY
	role_type ASC;
-- ----------------------------
-- 2.1 recommended videos from subscription
-- upto 50 records
-- ----------------------------
 SELECT
	s.user_id,
	v.title,
	v.file_path,
	(
	SELECT
		name
	FROM
		noob_tube.channel AS c
	WHERE
		c.id = s.channel_id) as channel_name
FROM
	noob_tube.subscription AS s
INNER JOIN noob_tube.channel_member AS cm ON
	cm.channel_id = s.channel_id
INNER JOIN video AS v ON
	v.channel_member_id = cm.id
WHERE
	s.user_id = @LoggedInUserID
ORDER BY
	v.upload_time DESC
LIMIT 50;
-- ----------------------------
-- 2.2 recommended videos from watch later
-- upto 50 records
-- ----------------------------
 SET
@LoggedInUserID = 140;

SELECT
	wl.user_id,
	wl.id AS watcher_later_id,
	v.title,
	v.file_path,
	(
	SELECT
		name
	FROM
		noob_tube.channel AS c
	INNER JOIN video AS v ON
		v.id = wl.video_id
	INNER JOIN channel_member cm ON
		v.channel_member_id = cm.id
	WHERE
		c.id = cm.channel_id ) as channel_name
FROM
	noob_tube.watch_later AS wl
INNER JOIN video AS v ON
	v.id = wl.video_id
WHERE
	wl.user_id = @LoggedInUserID
ORDER BY
	wl.creation_time ASC
LIMIT 50;