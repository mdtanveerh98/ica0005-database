
-- -------------------------------------
-- This contains the queires to show all the information for a "video" page. i.e it will display the video title, path, likes, comments etc
-- -------------------------------------
-- -------------------------------------
-- 1 video details
-- -------------------------------------
 SET
@CurrentVideoID = 101;

SELECT
	v.id,
	v.title,
	v.file_path,
	v.upload_time,
	(
	SELECT
		c.name
	FROM
		channel AS c
	INNER JOIN channel_member AS cm ON
		cm.channel_id = c.id
	WHERE
		cm.id = v.channel_member_id ) AS channel_name,
	(
	SELECT
		COUNT(*)
	FROM
		noob_tube.video_like AS vl
	WHERE
		vl.video_id = v.id) AS like_count
FROM
	noob_tube.video AS v
WHERE
	v.id = @CurrentVideoID;

-- -------------------------------------
-- 2 video comments
-- -------------------------------------
 SELECT
	vc.id,
	vc.video_id,
	vc.comment,
	vc.creation_time,
	(
	SELECT
		COUNT(*)
	FROM
		noob_tube.video_comment_like AS vcl
	WHERE
		vcl.comment_id = vc.id ) comment_like_count
FROM
	noob_tube.video_comment AS vc
WHERE
	vc.video_id = @CurrentVideoID;