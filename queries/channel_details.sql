
-- ------------------------------------------
-- This script contains te queries to view all the details about a channel
-- ------------------------------------------
-- ------------------------------------------
-- 1. Get the channel details, subscriber count and video count
-- ------------------------------------------
 SET
@CurrentChannelID = 27;

SELECT
	c.id,
	c.name,
	c.description,
	c.display_image_url,
	c.creation_time,
	(
	SELECT
		COUNT(*)
	FROM
		noob_tube.subscription AS s
	WHERE
		s.channel_id = c.id ) AS subscriber_count,
	(
	SELECT
		COUNT(*)
	FROM
		noob_tube.video AS v
	INNER JOIN noob_tube.channel_member AS cm ON
		cm.id = v.channel_member_id
	WHERE
		cm.channel_id = c.id ) AS video_count
FROM
	noob_tube.channel AS c
WHERE
	c.id = @CurrentChannelID;

-- ------------------------------------------
-- 2. Get channel members and roles
-- ------------------------------------------
 SELECT
	c.id,
	u.id,
	u.firstname,
	u.lastname,
	u.username,
	r.role_type
FROM
	noob_tube.channel AS c
INNER JOIN noob_tube.channel_member AS cm ON
	cm.channel_id = c.id
INNER JOIN noob_tube.`user` AS u ON
	u.id = cm.user_id
INNER JOIN noob_tube.`role` AS r ON
	r.id = cm.role_id
WHERE
	c.id = @CurrentChannelID;
	
-- ---------------------------------------
-- 3. Get latest videos of channel
-- ---------------------------------------
SELECT
	v.id,
	v.title,
	v.file_path
FROM noob_tube.video AS v
INNER JOIN channel_member AS cm ON
	v.channel_member_id = cm.id 
WHERE cm.channel_id = @CurrentChannelID
ORDER BY v.upload_time DESC
LIMIT 50;