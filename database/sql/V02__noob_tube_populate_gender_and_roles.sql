-- gender
INSERT INTO `noob_tube`.`gender` (id, value) VALUES (1, 'male');
INSERT INTO `noob_tube`.`gender` (id, value) VALUES (2, 'female');
INSERT INTO `noob_tube`.`gender` (id, value) VALUES (3, 'other');

-- role
INSERT INTO `noob_tube`.`role` (id, role_type) VALUES (1, 'admin');
INSERT INTO `noob_tube`.`role` (id, role_type) VALUES (2, 'content-creator');