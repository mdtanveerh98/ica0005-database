-- -----------------------------------------------------
-- Schema noob_tube
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `noob_tube` DEFAULT CHARACTER SET utf8mb4;
USE `noob_tube`;
-- -----------------------------------------------------
-- Table `noob_tube`.`gender`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `noob_tube`.`gender` (
	`id` INT NOT NULL,
	`value` VARCHAR(32) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `noob_tube`.`role`
-- This table defines the 'role' of users associated with channels
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `noob_tube`.`role` (
	`id` INT NOT NULL,
	`role_type` VARCHAR(32) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `noob_tube`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `noob_tube`.`user` (
	`id` INT NOT NULL,
	`firstname` VARCHAR(64) NULL,
	`lastname` VARCHAR(64) NULL,
	`username` VARCHAR(250) NOT NULL,
	`about` TEXT NULL,
	`email` VARCHAR(250) NOT NULL,
	`creation_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`display_image_url` VARCHAR(250) NULL,
	`gender_id` INT NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `UQ_user_email` UNIQUE INDEX (`email`),
	CONSTRAINT `UQ_user_username` UNIQUE INDEX (`username`),
	CONSTRAINT `FK_user_gender_id` FOREIGN KEY (`gender_id`) REFERENCES `noob_tube`.`gender`(`id`) ON
DELETE RESTRICT ON
	UPDATE RESTRICT
) ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `noob_tube`.`channel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `noob_tube`.`channel` (
	`id` INT NOT NULL,
	`name` VARCHAR(250) NOT NULL,
	`description` TEXT NULL,
	`creation_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`display_image_url` VARCHAR(250) NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `UQ_channel_name` UNIQUE INDEX (`name`)
) ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `noob_tube`.`channel_member`
-- This table is to check if a particular has rights to upload content to a particualar channel
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `noob_tube`.`channel_member` (
	`id` INT NOT NULL,
	`user_id` INT NULL,
	`channel_id` INT NOT NULL,
	`role_id` INT NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `UQ_channel_member_user_channel_role` UNIQUE INDEX (`user_id`, `channel_id`, `role_id`),
	-- user has only one role in the channel
	CONSTRAINT `FK_channel_member_user_id` FOREIGN KEY (`user_id`) REFERENCES `noob_tube`.`user` (`id`) ON
DELETE
	SET NULL -- set null if the user is deleted.
		ON
	UPDATE RESTRICT,
		CONSTRAINT `FK_channel_member_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `noob_tube`.`channel` (`id`) ON
		DELETE CASCADE ON
			UPDATE RESTRICT,
		CONSTRAINT `FK_channel_member_role_id` FOREIGN KEY (`role_id`) REFERENCES `noob_tube`.`role` (`id`) ON
				DELETE RESTRICT -- prevent roles from being updated or deleted once db is initalized
		ON
					UPDATE RESTRICT
) ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `noob_tube`.`video`
-- Table to contain the location of videos, additional metadata and ownership info (channel and uploader)
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `noob_tube`.`video` (
	`id` INT NOT NULL,
	`title` TEXT NOT NULL,
	`file_path` VARCHAR(250) NOT NULL,
	`description` MEDIUMTEXT NULL,
	`upload_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`channel_member_id` INT NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `UQ_video_file_path` UNIQUE INDEX (`file_path`),
	CONSTRAINT `FK_video_channel_member_id` FOREIGN KEY (`channel_member_id`) REFERENCES `noob_tube`.`channel_member` (`id`) ON
DELETE CASCADE ON
	UPDATE RESTRICT
) ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `noob_tube`.`subscription`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `noob_tube`.`subscription` (
	`id` INT NOT NULL,
	`subscription_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`user_id` INT NOT NULL,
	`channel_id` INT NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `UQ_subscription_user_id_channel_id` -- user can be subscribed to a channel only once
	UNIQUE INDEX (`user_id`, `channel_id`),
	CONSTRAINT `FK_subscription_user_id` FOREIGN KEY (`user_id`) REFERENCES `noob_tube`.`user` (`id`) ON
DELETE CASCADE -- delete subscription if user is removed
	ON
	UPDATE RESTRICT,
	CONSTRAINT `FK_subscription_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `noob_tube`.`channel` (`id`) ON
		DELETE CASCADE -- delete subscription if channel is removed
	ON
			UPDATE RESTRICT
) ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `noob_tube`.`watch_later`
-- Table to contain which video user has saved to watch later.
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `noob_tube`.`watch_later` (
	`id` INT NOT NULL,
	`creation_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`user_id` INT NOT NULL,
	`video_id` INT NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `UQ_watch_later_user_id_video_id` -- user can be add a video to watch later only once
	UNIQUE INDEX (`user_id`, `video_id`),
	CONSTRAINT `FK_watch_later_video_id` FOREIGN KEY (`video_id`) REFERENCES `noob_tube`.`video` (`id`) ON
DELETE CASCADE -- delete if video removed
	ON
	UPDATE RESTRICT,
	CONSTRAINT `FK_watch_later_user_id` FOREIGN KEY (`user_id`) REFERENCES `noob_tube`.`user` (`id`) ON
		DELETE CASCADE -- delete if user is removed
	ON
			UPDATE RESTRICT
) ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `noob_tube`.`video_comment`
-- Table to contain comments on a particular video
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `noob_tube`.`video_comment` (
	`id` INT NOT NULL,
	`comment` MEDIUMTEXT NOT NULL,
	`creation_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`user_id` INT NULL,
	`video_id` INT NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_video_comment_video_id` FOREIGN KEY (`video_id`) REFERENCES `noob_tube`.`video` (`id`) ON
DELETE CASCADE -- delete if video removed
	ON
	UPDATE RESTRICT,
	CONSTRAINT `FK_video_comment_user_id` FOREIGN KEY (`user_id`) REFERENCES `noob_tube`.`user` (`id`) ON
		DELETE CASCADE -- delete comment if user is removed
	ON
			UPDATE RESTRICT
) ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `noob_tube`.`video_like`
-- Table to contain which user likes which video
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `noob_tube`.`video_like` (
	`id` INT NOT NULL,
	`creation_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`user_id` INT NOT NULL,
	`video_id` INT NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `UQ_video_like_user_id_video_id` -- user can like a video only once
	UNIQUE INDEX (`user_id`, `video_id`),
	CONSTRAINT `FK_video_like_video_id` FOREIGN KEY (`video_id`) REFERENCES `noob_tube`.`video` (`id`) ON
DELETE CASCADE -- delete if video removed
	ON
	UPDATE RESTRICT,
	CONSTRAINT `FK_video_like_user_id` FOREIGN KEY (`user_id`) REFERENCES `noob_tube`.`user` (`id`) ON
		DELETE CASCADE -- delete if user is removed
	ON
			UPDATE RESTRICT
) ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `noob_tube`.`video_comment_like`
-- Table to contain which user likes which video
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `noob_tube`.`video_comment_like` (
	`id` INT NOT NULL,
	`creation_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`user_id` INT NOT NULL,
	`comment_id` INT NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `UQ_video_comment_like_user_id_comment_id` -- user can like a video-comment only once
	UNIQUE INDEX (`user_id`, `comment_id`),
	CONSTRAINT `FK_video_comment_like_comment_id` FOREIGN KEY (`comment_id`) REFERENCES `noob_tube`.`video_comment` (`id`) ON
DELETE CASCADE -- delete if comment is removed
	ON
	UPDATE RESTRICT,
	CONSTRAINT `FK_video_comment_like_user_id` FOREIGN KEY (`user_id`) REFERENCES `noob_tube`.`user` (`id`) ON
		DELETE CASCADE -- delete if user is removed
	ON
			UPDATE RESTRICT
) ENGINE = InnoDB;