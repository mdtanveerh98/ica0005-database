#!/bin/sh
######################
# bash script used inside the flyway container to continiously poll the db server until server is accepting connection
# so that flyway doesnt automatically fail when db server is not available initally.
######################
set -e
  
host="$1"
shift
cmd="$@"
  
until mysql --host="$host" -e '\q'; do
  sleep 5
done
  
>&2 echo "************* mysql is up - executing command : ${cmd} **********************"
exec $cmd 