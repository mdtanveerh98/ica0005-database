#flyway/flyway:7.3.1
FROM flyway/flyway@sha256:bf5abc6403fa4f2e422fc9a2f62ba72d7f41a907404310caa4bfafe5ca217f97
USER root
RUN apt-get update && \
    apt-get install -y mysql-client-5.7

USER flyway
RUN  mkdir /flyway/bin

ADD ./database/bin/wait.sh /flyway/bin/wait.sh
ADD ./database/mysql/my.cnf /etc/mysql/my.cnf

USER root
RUN chmod 755 /flyway/bin/wait.sh
RUN chmod 0444 /etc/mysql/my.cnf
