## Assignment 5
The scripts that is used to populate the database is in the `database\sql` directory.

The scripts used to query the data from the databasae is in the `queries` directory.



### Running the database
This repository contains a docker-composee file. To run this one must have docker and docker-compose installed. Use the command `docker-compose up` on the project root to create the run the database container. Docker and Docker compose is used to automate the database creation and initalization process. One the database container is running (using the previously mentioned command) the database container can be accesed via the regular mysql port 3306. The username and password are specified in the docker-compose file

If does not have docker/docker-compose installed or choses to run the scripts on a mysql server running on the host machine, the database initalization scripts can be found in the aforementioned `database\sql` directory. The scripts are prefixed with `VXX` number to specify the order in which the scripts should be run. 


### Additional files
This repository contains additional files to automate the process of creating the database in a docker container. These additional files are configuartion files (for mysql and fly) and scripts so that the container startup process is correct.